package net.xeill.elpuig;
/*
*Clase IA
* Generamos un competidor para poder jugar contra alguien en la Oca, para ello hemos copiado los atributos de jugador con 
* la declaración "extends" que nos permite crear una "hija" de otro clase, en este caso de jugador.
*
*  @author fran_
*/

public class IA extends Jugador{
  public IA(String nombre) {
    super(nombre);
  
  }
  
public IA() {
}
  public void sayHello() {
    System.out.println("Hola, mi nombre es "+nombre+"\n");
  }

}
