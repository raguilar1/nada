package net.xeill.elpuig;
/*
*Clase Dado, 
* Creamos un dado Random para poder lanzar números y asi poder avanzar.
*
*  @author fran_
*
*/

public class Dado {
    public static int getNumeroAleatorio(int minimo,int maximo){
        int num=(int)Math.floor(Math.random()*(maximo-minimo+1)+(minimo));
        return num;

    }
}
